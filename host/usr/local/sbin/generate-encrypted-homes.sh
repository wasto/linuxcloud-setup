#!/usr/bin/sh
#Erwarteter Parameter: Gesamtzahl der Userhomes die es nachher geben soll.
#(Nicht Anzahl neu zu erstellender User!)
#
encryptedHomesDir="/encryptedhomes"
decryptedHomesDir="/decryptedhomes"
keyfilesDir="/keyfiles"
keyLength=50
firstUID=1000
lastUID=$(($1 + firstUID - 1))
imageSize=2048 # in MB

for uid in $(seq $firstUID $lastUID); do
  if grep -e "$uid".img /etc/fstab >/dev/null; then
    continue
  fi
  dd if=/dev/zero of="$encryptedHomesDir/images/$uid".img bs=1M count="$imageSize"
  mkfs.ext4 "$encryptedHomesDir/images/$uid".img
  mkdir "$encryptedHomesDir/mount/$uid"
  echo "$encryptedHomesDir/images/$uid.img $encryptedHomesDir/mount/$uid" \
    ext4 loop,defaults,_netdev 0 2 >>/etc/fstab
  mount "$encryptedHomesDir/mount/$uid"
  mkdir "$encryptedHomesDir/mount/$uid/$uid"
  chmod -R 777 "$encryptedHomesDir/mount/$uid"
  lxc config device add euler "$uid" disk \
    source="$encryptedHomesDir/mount/$uid/$uid" \
    path="$encryptedHomesDir/$uid" \
    readonly=false
  lxc exec euler -- \
    lxc config device add manager "$uid" disk \
    source="$encryptedHomesDir/$uid" \
    path="$encryptedHomesDir/$uid" \
    readonly=false
  lxc exec euler -- \
    lxc exec manager -- \
    openssl rand -out "$keyfilesDir/$uid" -base64 "$keyLength"
  lxc exec euler -- \
    lxc exec manager -- \
    gocryptfs -passfile "$keyfilesDir/$uid" \
    -config "$keyfilesDir/$uid".conf \
    -init "$encryptedHomesDir/$uid"
  lxc exec euler -- \
    lxc exec manager -- \
    mkdir "$decryptedHomesDir/$uid"
  lxc exec euler -- \
    lxc exec manager -- \
    gocryptfs -passfile "$keyfilesDir/$uid" \
    -config "$keyfilesDir/$uid".conf \
    -force_owner "$uid:$uid" \
    "$encryptedHomesDir/$uid" \
    "$decryptedHomesDir/$uid"
  #entr doesn't work over sshfs!
  #We need to update the desktop mounts now!
  lxc exec euler -- \
    lxc exec manager -- sh -c \
     "ls \"$encryptedHomesDir/\" >/etc/homelist"
  for container in deployed master testing; do
    lxc exec euler -- \
      lxc exec "$container" update-storage.sh
  done 
done
