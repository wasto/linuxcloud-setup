Haftungsausschluss für die Nutzung unseres Online-Dienstes zur Bereitstellung
eines virtuellen Desktops im Web:

Bitte lesen Sie diesen Haftungsausschluss sorgfältig, bevor Sie unseren Dienst
nutzen. Durch die Nutzung unseres Dienstes erklären Sie sich mit den folgenden
Bedingungen einverstanden:

ACHTUNG: Wenn ein Benutzerkonto 30 Tage lang nicht benutzt wird, werden es und
seine Daten automatisch gelöscht. Dies ist aus Kapazitätsgründen notwendig!

Zweck der Dienstleistung: Unser Online-Dienst bietet die Möglichkeit, einen
virtuellen Desktop über das Web zu nutzen. Diese Dienstleistung ist
ausschliesslich für legale und rechtmässige Zwecke vorgesehen.

Verbotene Aktivitäten: Die Nutzung unseres Dienstes zu illegalen, schädlichen,
betrügerischen, belästigenden, bedrohlichen oder anderweitig unangemessenen
Zwecken ist strengstens untersagt. Jegliche Aktivitäten, die gegen geltende
Gesetze, Vorschriften oder Rechte Dritter verstossen, sind nicht gestattet.

Protokollierung und Datenweitergabe: Wir behalten uns das Recht vor, relevante
Log-Daten über die Nutzung unseres Dienstes zu erfassen. Diese Daten können im
Falle eines Verdachts auf illegale Aktivitäten oder Verstösse gegen unsere
Nutzungsbedingungen an die entsprechenden Behörden weitergegeben werden, um die
Einhaltung der Gesetze sicherzustellen und die Sicherheit unseres Dienstes zu
gewährleisten.

Nutzungsverantwortung: Die Nutzer sind allein verantwortlich für ihre
Aktivitäten im Zusammenhang mit der Nutzung unseres Dienstes. Jegliche
rechtswidrige oder missbräuchliche Nutzung kann zu rechtlichen Konsequenzen
führen.

Datenschutz: Persönliche Daten, mit Ausnahme Ihres gültigen Benutzernamens der Schule, 
die diesen Dienst verwendet, werden weder gespeichert, noch gesammelt, noch sind sie
notwendig. Logdateien werden nur im Falle eines Verdachts auf illegale Aktivitäten 
und ansonsten nur solange technisch notwendig (zum Beispiel zur Beseitigung von Bugs) 
aufbewahrt. 
Sämtliche Arbeitsdokumente und Einstellungen, die Sie innerhalb des virtuellen Desktops 
anlegen, werden dort verschlüsselt abgelegt. Die Ausnahmen von diesem Grundsatzes sind:
1) Wenn Sie externe Cloudspeicher nutzen, so haben wir über eine allfällig dort vorhandene
oder nicht vorhandene Verschlüsselung keine Kontrolle.
2) Es ist technisch nicht möglich zu verhindern, dass jemand mit Administratorenrechten
auf dem Server grundsätzlich die Verschlüsselung aushebeln könnte.

Keine Gewährleistung: Wir übernehmen keine Gewährleistung für die
Verfügbarkeit, Zuverlässigkeit oder Leistungsfähigkeit unseres Dienstes. Die
Nutzung erfolgt auf eigene Gefahr.

Änderungen am Haftungsausschluss: Wir behalten uns das Recht vor, diesen
Haftungsausschluss jederzeit zu ändern oder zu aktualisieren. Es liegt in Ihrer
Verantwortung, regelmässig nachzusehen, ob Änderungen vorgenommen wurden. Die
fortgesetzte Nutzung unseres Dienstes nach solchen Änderungen gilt als
Zustimmung zu den aktualisierten Bedingungen.

Durch die Nutzung unseres Dienstes erklären Sie sich mit diesen Bedingungen und
Bestimmungen einverstanden. Wenn Sie nicht mit diesen Bedingungen einverstanden
sind, nutzen Sie unseren Dienst bitte nicht. 
