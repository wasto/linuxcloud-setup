#!/bin/sh
title="Willkommen"
disclaimerText="/var/local/disclaimer.txt"

kdialog --title="$title" --textbox "$disclaimerText" 400 400
kdialog --title-"$title" --msgbox="
Herzlich Willkommen auf dem linuxcloud Desktop!

Bitte ändern Sie Ihr Passwort. Sie werden dazu aufgefordert, wenn Sie
dieses Fenster schließen.
"

change-password.sh && rm "$HOME/.config/autostart/welcome.sh.desktop"
