#!/bin/sh

remoteName="onedrive"

if rclone listremotes | grep -q "^$remoteName:"; then
  kdialog --warningyesno \
    "Es gibt bereits einen Eintrag Namens $remoteName bei rclone. \n
  Möchten Sie diesen Löschen und neu erstellen?" &&
    rclone config delete "$remoteName" || exit 1
fi

rclone --onedrive-drive-type business config create "$remoteName" onedrive
systemctl --user enable onedrive-mount.service
systemctl --user start onedrive-mount.service &&
  rm "$HOME/Desktop/setup-onedrive-mount.desktop"
