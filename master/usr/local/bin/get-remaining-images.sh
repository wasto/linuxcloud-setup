#!/bin/sh
(
  awk -F: '($3 >= 1000 && $3 < 65534) {printf "%s\n",$3}' /var/lib/extrausers/passwd
  ls -1 /decryptedhomes/
) | sort | uniq -u | wc -l
