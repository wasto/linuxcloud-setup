#!/bin/sh
userID=$(id -u "$PAM_USER")

if [ "$PAM_TYPE" = "close_session" ]; then
  if who | grep -wq "$PAM_USER"; then
    echo "user $PAM_USER is still logged in. Aborting"
    exit 1
  else
    if [ "$userID" -lt 1000 ]; then
      echo "UID $userID of user $PAM_USER is too low. Aborting."
      exit 1
    else
      for fuserMount in \
        $(mount | grep fuse | grep "user_id=$userID" | cut -d' ' -f3); do
        sudo -u "$PAM_USER" fusermount -uz "$fuserMount"
      done
      exit 0
    fi
  fi
fi
