#!/bin/sh
remoteHost="manager"
decryptedHomesDir="/decryptedhomes"
( mount | grep "decryptedhomes/" | cut -f1 -d" " | cut -f4 -d"/"
cat /var/lib/extrausers/homelist ) | sort | uniq -u | while read image; do
  (ls -dq "$decryptedHomesDir/$image" >/dev/null 2>/dev/null) ||
    mkdir -p "$decryptedHomesDir/$image"
  mount | grep "decryptedhomes/$image" >/dev/null 2>/dev/null ||
    nohup sshfs -o StrictHostKeyChecking=no \
      -o allow_other -o default_permissions \
      -o IdentityFile=/root/.ssh/id_rsa \
      root@"$remoteHost":"$decryptedHomesDir/$image" \
      "$decryptedHomesDir/$image" > /dev/null
done
