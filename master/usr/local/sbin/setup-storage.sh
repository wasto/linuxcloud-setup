#!/bin/sh

remoteHost="manager"
#Set up user-mapping.xml, making sure only root can access it:
sshfs -o StrictHostKeyChecking=no,uid=0,gid=0,allow_other,default_permissions \
  root@"$remoteHost":/var/guacamoleusers/ /var/guacamoleusers/

#Set up extrausers
sshfs -o StrictHostKeyChecking=no -o allow_other -o follow_symlinks -o default_permissions \
  -o IdentityFile=/root/.ssh/id_rsa \
  root@"$remoteHost":/etc /var/lib/extrausers/

update-storage.sh
