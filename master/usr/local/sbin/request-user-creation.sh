#!/bin/bash

if [ $# -eq 0 ]; then
  echo "Usage: $0 <username>"
  echo "You can also pipe the desired password into the command"
  exit 1
fi

publicKey="/root/.ssh/linuxcloud-public.pem"
requestDir="/var/lib/extrausers/create-user-requests"
userName="$1"
timeStamp=$(date +%s)
requestFile=$(mktemp "${timeStamp}XXXXXX")
read -rsp "Passwort for user $userName?" password
printf "$userName\t$password" | openssl pkeyutl -encrypt -inkey "$publicKey" \
  -pubin -out "$requestFile"
mv "$requestFile" "$requestDir"
