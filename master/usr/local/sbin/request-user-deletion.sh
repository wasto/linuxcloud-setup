#!/bin/sh

userName="$1"
tempDir=$(mktemp -d)
tempRequestFile="$tempDir/$userName"
requestDir="/var/lib/extrausers/delete-user-requests"
requestFile="$requestDir/$userName"
loginTimesFile="/var/lib/extrausers/login-times"

userID=$(id -u "$userName") || (echo "User $userName does not exist." && exit 1)
pkill -9 -U "$userID"
sed -i "/\b$userName\b/d" "$loginTimesFile"
echo "$userName" >"$tempRequestFile"
chown "$userName" "$tempRequestFile"
mv "$tempRequestFile" "$requestFile"
rm -r "$tempDir"
exit 0
