#!/bin/sh
userMappingFile="/var/guacamoleusers/user-mapping.xml"
userList=$(grep -o 'username="[^"]*"' "$userMappingFile" | sed 's/username="//;s/"//')
title="Verwaltung von Nutzerkonten"
validUsernamePattern="^[a-z][a-z0-9_-]{0,31}$"

choose() {
  choice=$(kdialog --title "$title" --menu "Bitte wählen Sie eine Option" \
    create-user "Konto erstellen" \
    change-password "Passwort eines Kontos ändern" \
    delete-user "Nutzer löschen") && return 0
  return 1
}

createUser() {
  if echo "$userName" | grep -qE "$validUsernamePattern"; then
    newPassword=$(kdialog --title="$title" \
      --newpassword="Geben Sie das gewünschte Passwort ein.") &&
      echo "$newPassword" | request-user-creation.sh "$userName" &&
      kdialog --title="$title" \
        --msgbox "Es wurde eine Anfrage für das Konto $userName an \
den Server geschickt!" && exit 0
  else
    kdialog --error "Ungültiger Benutzername"
  fi
}

deleteUser() {
  if echo "$userName" | grep -qE "$validUsernamePattern"; then
    kdialog --title "$title" --yesno \
      "Sind Sie sicher, dass sie das Konto von $userName löschen möchten?" &&
      request-user-deletion.sh "$userName" &&
      kdialog --title="$title" --msgbox \
        "Es wurde eine Anfrage zur Löschung des Kontos von $userName \
an den Server geschickt!" && exit 0
  else
    kdialog --error "Ungültiger Benutzername"
  fi
}

while choose; do
  case "$choice" in
  "create-user")
    userName=$(kdialog --title="$title" \
      --inputbox "Bitte geben Sie den gewünschten Benutzernamen ein.") &&
      createUser
    ;;
  "delete-user")
    userName=$(kdialog --combobox \
      "Bitte wählen Sie das zu löschende Konto aus." $userList) &&
      deleteUser
    ;;
  "change-password")
    userName=$(kdialog --combobox \
      "Bitte wählen Sie das Konto aus für dass Sie das Passwort \
neu setzen möchten." $userList) &&
      createUser
    ;;
  esac
done
