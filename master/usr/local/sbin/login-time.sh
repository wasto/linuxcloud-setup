#!/bin/sh

logFile="/var/lib/extrausers/login-times"

userID=$(id -u "$PAM_USER")
if [ "$userID" -lt 1000 ]; then
  echo "UID $userID is too low." >&2
else
  sed -i "/\b$PAM_USER\b/d" "$logFile"
  timestamp=$(date "+%s")
  echo "$timestamp $PAM_USER" >>"$logFile"
fi
