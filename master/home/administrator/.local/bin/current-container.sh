#!/bin/bash
case "$HOSTNAME" in
"testing")
  echo "Sie befinden sich im Testing-Container."
  echo "Änderungen bleiben temporär bis Sie den"
  echo "Master-Container neu generieren!"
  echo "Löschen des Testing-Containers ohne zuvor"
  echo "den Master-Container neu zu generieren,"
  echo "macht alle hier durchgeführten Änderungen"
  echo "rückgängig."
  ;;
"master")
  echo "Sie befinden sich im Master-Container."
  echo "Hier sollten Sie nur in Ausnahmefällen"
  echo "arbeiten!"
  echo "Der normale Workflow besteht darin, zuerst"
  echo "einen Testing-Container zu generieren, darin"
  echo "allfällige Änderungen durchzuführen und zu"
  echo "testen, und anschliessend den Master-Container"
  echo "neu zu generieren."
  ;;
"deployed")
  echo "Sie befinden sich im von den Nutzerinnen und"
  echo "Nutzern aktiv im Gebrauch befindlichen Deployed-"
  echo "Container!"
  echo "Normalerweise sollten Sie hier keine Änderungen"
  echo "durchführen, ausser den Nutzerinnen und Nutzern"
  echo "direkt zu helfen."
  ;;
*)
  echo "Unbekannter Hostname: $HOSTNAME"
  ;;
esac
