server {
    #listen 80 default_server;
    root /var/www/html;
    index index.html index.htm index.nginx-debian.html;
    server_name guacamole.linuxcloud.ch;
    location / {
        proxy_pass http://localhost:8080/guacamole/;
        proxy_buffering off;
        proxy_http_version 1.1;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection $http_connection;
        access_log off;
    }
    location /status {
		#/var/local/guacamoleusers should be sshfs mounted from
		#manager, using -o allow_other, -o default_permissions
	    	#and -o follow_symlinks
            alias /var/local/guacamoleusers/status/;
    }
    listen 443 ssl;
#    ssl_certificate      /etc/nginx/ssl/cert/guacamole.linuxcloud.ch.crt;
#    ssl_certificate_key  /etc/nginx/ssl/private/guacamole.linuxcloud.ch.key;
    ssl_certificate     /certificates/fullchain.pem;
    ssl_certificate_key /certificates/privkey.pem;
    ssl_session_cache shared:SSL:1m;
    ssl_session_timeout  5m;
}
server {
    return 301 https://$host$request_uri;
    listen 80 default_server;
    root /var/www/html;
    index index.html index.htm index.nginx-debian.html;
    server_name guacamole.linuxcloud.ch;
    location / {
        proxy_pass http://localhost:8080/guacamole/;
        proxy_buffering off;
        proxy_http_version 1.1;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection $http_connection;
        access_log off;
    }
}

