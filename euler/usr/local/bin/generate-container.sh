#!/bin/sh

targetContainer="$1"
sourceContainer="master"
case $targetContainer in
master)
  sourceContainer="testing"
  rdp=3389
  ;;
testing)
  rdp=3388
  ;;
deployed)
  rdp=3387
  ;;
*)
  exit 1
  ;;
esac

lxc info "$sourceContainer" >/dev/null 2>/dev/null || exit 1
lxc exec "$targetContainer" -- wall -n \
  "ACHTUNG: Dieses System wird in 5 Minuten \
(um $(date -d '+5 minutes' '+%H:%M')) neu gestartet. \
Bitte speichern Sie ihre Dokumente!"
sleep 4m
lxc exec "$targetContainer" -- wall -n \
  "ACHTUNG: Dieses System wird einer Minute \
(um $(date -d '+1 minutes' '+%H:%M')) neu gestartet. \
Bitte speichern Sie ihre Dokumente!"
sleep 1m
lxc stop "$targetContainer"
lxc delete "$targetContainer"
lxc copy "$sourceContainer" "$targetContainer"
lxc config device set "$targetContainer" \
  rdp-forward listen=tcp:0.0.0.0:$rdp connect=tcp:127.0.0.1:3389
lxc start "$targetContainer"
