#!/bin/sh

requestDir="/etc/reset-user-requests"
finishedRequests="$requestDir/.meta/finished-requests"

if [ -z "$(ls $requestDir)" ]; then
  echo "Keine Anfragen gefunden."
  exit 0
fi

for requestFile in "$requestDir"/*; do
  userName=$(cat "$requestFile")
  userID=$(lxc exec manager -- id -u "$userName")
  if [ "$userID" -lt 1000 ]; then
    echo "UID $userID is too low. Not deleting $userName." >&2
  else
	  reset-user.sh "$userName"
  fi
  basename "$requestFile" >>"$finishedRequests"
  rm "$requestFile"
done
