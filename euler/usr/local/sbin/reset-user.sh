#!/bin/sh

userName="$1"
userID=$(lxc exec manager -- id -u "$userName") || exit 1
homeDir="/decryptedhomes/$userID/$userName"

for container in deployed master testing; do
	lxc exec "$container" -- pkill -9 -U "$userID"
done
lxc exec manager -- rsync -aL --del /etc/skel/.config/ "$homeDir/.config/"
	
