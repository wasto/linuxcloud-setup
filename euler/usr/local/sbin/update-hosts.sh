#!/bin/sh

hostsFile="/etc/hosts"
containerList=$(lxc list status=running --format csv --columns=n)
ipv4Regex='([0-9]{1,3}[\.]){3}[0-9]{1,3}'

setIP() {
  ipAddress="$1"
  host="$2"
  echo "$ipAddress" | grep -qE "$ipv4Regex" ||
    (
      echo "Host $host not ready. Got an invalid IP Adress: $ipAddress"
      return 1
    )
  grep -wq "$host" "$hostsFile" || echo "$ipAddress $host" >>"$hostsFile"
  sed -E -i "s/($ipv4Regex)[[:space:]]\b$host\b/$ipAddress $host/" \
    "$hostsFile"
  for cnt in $containerList; do
    lxc exec "$cnt" -- sh -c \
      "grep -wq $host $hostsFile || echo $ipAddress $host >> $hostsFile"
    lxc exec "$cnt" -- \
      sed -E -i "s/($ipv4Regex)[[:space:]]\b$host\b/$ipAddress $host/" \
      "$hostsFile"
  done
  return 0
}

hostName=$(hostname)
defaultNetworkDevice=$(ip route | awk '/default/ {print $5}')
localIP=$(ip addr show "$defaultNetworkDevice" |
  grep -oP '(?<=inet\s)\d+(\.\d+){3}')
setIP "$localIP" "$hostName"

exit_code=0
for container in $containerList; do
  containerIP=$(lxc list "$container" --format=csv --columns=4 | cut -d' ' -f1)
  setIP "$containerIP" "$container" || exit_code=1
done
exit "$exit_code"
