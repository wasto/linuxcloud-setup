#!/bin/sh

requestDir="/etc/reset-user-requests"
finishedRequests="$requestDir/.meta/finished-requests"
webAppURL="https://webapp-gymms.sbl.ch/linuxcloud/reset-requests"
tempDir=$(mktemp -d)

(
  curl --insecure "$webAppURL/list"
  cat "$finishedRequests"
) | sort |
  uniq -u | xargs -n 1 -I {} -P 5 \
  curl --insecure "$webAppURL/{}" -o "$tempDir/{}"
cp "$tempDir"/* "$requestDir" &&
rm -r "$tempDir"
process-reset-requests.sh
