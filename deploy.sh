#!/bin/sh
sourceDir=${1%/}
ls "$sourceDir" >/dev/null 2>/dev/null || exit 1
stow --target=/ --no-folding --dir="$sourceDir" --adopt -S .
ldconfig
