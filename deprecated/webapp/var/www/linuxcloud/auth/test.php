<?php

$domain = "linuxcloud.ch";
$username = $_SERVER['PHP_AUTH_USER'];

//Momentan fällt mir nichts besseres ein um zu verhindern, dass das passwort slashes oder pluszeichen enthält
//Dies könnte für schwierigkeiten sorgen
$password = shell_exec("openssl rand -base64 9 | tr '/' 'I' | tr '+' 'p'");
$tempname = shell_exec('mktemp --tmpdir=../requests/');
$timestamp = trim(shell_exec('date +%s'));
$finalname = preg_replace("/tmp/", $timestamp, $tempname);
$finalname = preg_replace("/ /", "", $finalname);
shell_exec("rm $tempname");
$file = basename($finalname);

shell_exec("echo -n \"$username $password\" | openssl pkeyutl -encrypt -inkey publicKey.pem -pubin -out $finalname");
shell_exec("echo -n \"$file\" >> ../requests/list");

echo "Der Account auf https://$domain wird eingerichtet. Es kann bis zu 5 Minuten dauern, bis alles bereit ist!<br>";
echo "Danach können Sie sich auf <a href=\"https://$domain\">https://$domain</a> einloggen.<br>";
echo "Username = $username<br>";
echo "Passwort = $password";
echo "<p>";
echo "Schreiben Sie sich das Passwort auf oder kopieren Sie es! Es wird nur solange angezeigt bis Sie die Seite verlassen! Sollten Sie diese Seite neu laden, so wird ein neues Passwort generiert!";
echo "<p>";
echo "Falls das nicht funktioniert, so ist der Server gerade nicht erreichbar oder es gibt zur Zeit keinen freien Platz mehr für einen neuen Account. Versuchen Sie es in diesem Fall später nochmals.";
echo "<p>";
echo "Sie können den aktuellen Status der Accounterstellung hier abfragen: <a href=\"status.php\">Status-Seite</a>";
?>

