<?php

$domain = "status.linuxcloud.ch";
$username = $_SERVER['PHP_AUTH_USER'];

//--no-check-certificate ist notwendig solange die letsencrypt-Zertifikate nicht auch für
//status.linuxcloud.ch gelten. Das hat keine Priorität.

$status = shell_exec("wget -q -O - https://$domain/status/$username --timeout=10 --no-check-certificate | cat");

if ($status == ""){
        $status = "Request gesendet, Server wird ihn innerhalb der nächsten Minute abholen.";
}


echo $status; 

echo "<br>Laden Sie diese Seite neu, um den Status zu aktualisieren.";

?>

