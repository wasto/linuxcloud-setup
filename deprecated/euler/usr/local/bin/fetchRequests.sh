#!/bin/sh

#Notizen:
#Keys wurden erzeugt mit:
# openssl genpkey -algorithm RSA -out myPrivate.key \
# -pkeyopt rsa_keygen_bits:2048
# openssl rsa -pubout -in myPrivate.key -out publicKey.pem

#myPrivate.key muss sich nachher in $requestDir" befinden publicKey.pem muss
#auf den webapp kopiert werden, zur Zeit unter /var/www/linuxcloud/auth/

requestDir="/home/lxadmin/requests"
newTestUsersDir="/home/lxadmin/newtestusers"
webAppURL="https://webapp-gymms.sbl.ch/linuxcloud/requests"
currentList="$requestDir/currentlist"
finishedRequests="$requestDir/finishedrequests"
privateKey="$requestDir/myPrivate.key"

curl "$webAppURL/list" -o "$currentList"
cat "$currentList" "$finishedRequests" | sort | uniq -u |
  while read -r line; do
    curl "$webAppURL/$line" -o "$requestDir/$line"
    openssl pkeyutl -decrypt -inkey "$privateKey" \
      -in "$requestDir/$line" -out "$newTestUsersDir/$line"
    echo "$line" >>"$finishedRequests"
    rm "$requestDir/$line"
  done
