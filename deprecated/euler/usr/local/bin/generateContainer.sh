#!/bin/sh

targetContainer="$1"
sourceContainer="master"
case $targetContainer in
master)
  sourceContainer="testing"
  ipsuffix=2
  rdp=3389
  ;;
testing)
  ipsuffix=4
  rdp=3388
  ;;
deployed)
  ipsuffix=6
  rdp=3387
  ;;
*)
  exit 1
  ;;
esac

lxc info "$sourceContainer" >/dev/null 2>/dev/null || exit 1
lxc stop "$targetContainer"
lxc delete "$targetContainer"
lxc copy "$sourceContainer" "$targetContainer"
lxc config device set "$targetContainer" \
  rdp-forward listen=tcp:0.0.0.0:$rdp connect=tcp:127.0.0.1:3389
lxc config device set "$targetContainer" eth0 ipv4.address=10.167.130.$ipsuffix
lxc start "$targetContainer"
