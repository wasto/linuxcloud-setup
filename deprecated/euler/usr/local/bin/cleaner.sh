#!/bin/bash

#Dieses Script sollte per Cron z.B. einmal Nachts um 3 Uhr ausgeführt werden.

container="master" #Master oder doch eher testing? Testing müsste man zuerst erzeugen,
#man wohl nicht immer tut...

#Process user deletion requests
for request in $(lxc exec "$container" -- ls /delrequests); do
  user=$(lxc exec "$container" -- cat "/delrequests/$request")
  lxc exec accounts -- userdel -r "$user"
  lxc exec guacamole -- deleteUser.sh "$user"
  lxc exec "$container" -- rm "/delrequests/$request"
done

#Make a new clean deployed container out of master
generateContainer.sh deployed
