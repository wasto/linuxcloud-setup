#!/bin/sh

container="testing" #Später ändern zu deployed!
updateGuacDir="/updateguac"
seperator=" "

for file in $(lxc exec "$container" -- ls "$updateGuacDir"); do
  owner=$(lxc exec "$container" -- stat -c '%U' "$updateGuacDir/$file")
  passHash=$(lxc exec "$container" -- \
    cut -d"$seperator" -f1 "$updateGuacDir/$file")
  lxc exec guacamole -- updateguac.sh "$owner" "$passHash"
  lxc exec "$container" -- rm "$updateGuacDir/$file"
done
