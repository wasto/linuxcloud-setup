#!/bin/sh
container="testing" #später ändern zu deployed!
newUsersDir="/home/lxadmin/newusers"
logFile="/home/lxadmin/message"
encryptedHomesDir="/encryptedhomes"
decryptedHomesDirNoSlash="decryptedhomes"
decryptedHomesDir="/$decryptedHomesDirNoSlash"
statusDir="/var/www/html/status"
passwdFile="/etc/passwd"

tmpBaseDir="/root"
seperator=" "
updateGuacDir="/updateguac"
ipAdress="10.167.130.3"

abortUser() {
  if $isNewUser; then
    lxc exec accounts -- userdel -r "$user"
  fi
  lxc exec guacamole -- sh -c
  "echo Unbekannter Fehler. Bitte nochmals probieren. > $statusFile"
  return 1
}

if [ -z "$(ls -A -- "$newUsersDir")" ]; then
  exit
fi

for file in "$newUsersDir"/*; do
  user=$(cut -d"$seperator" -f1 "$file")
  lxc exec accounts -- id "$user" && isNewUser=false || isNewUser=true
  statusFile="$statusDir/$user"
  lxc exec guacamole -- sh -c \
    "echo Account wird erstellt oder bearbeitet > $statusFile"
  password=$(cut -d"$seperator" -f2- "$file")
  hashedPW=$(printf %s "$password" | sha256sum)
  nextUID=$(lxc exec accounts -- sh -c \
    "(cut -d: -f3 $passwdFile | grep "^[1-9]..."; seq 1000 9999) | \
	  sort -n | uniq -u | head -1")
  lxc exec accounts -- sh -c \
    "echo $user:$password:$nextUID:$nextUID::/home/$user:/bin/bash | \
    newusers" || abortUser || continue
  uid=$(lxc exec accounts -- id -u "$user") || abortUser || continue

  if [ -e "$encryptedHomesDir/$uid" ]; then
    #Move the user home into his encrypted container, making sure
    #it's empty beforehand
    if $isNewUser; then
      lxc exec "$container" -- sh -c "rm -fr $encryptedHomesDir/$uid/*" &&
        lxc exec "$container" -- mkdir "$encryptedHomesDir/$uid/$user" &&
        lxc exec "$container" -- \
          chown -R "$user:$user" "$encryptedHomesDir/$uid/$user" &&
        #lxc exec $container --
        #       dd if=/dev/zero of="$encryptedHomesDir/$uid/filler"
        #       2>/dev/null #Too slow :-(
        #lxc exec $container -- rm $encryptedHomesDir/$uid/filler
        lxc exec "$container" -- \
          rsync -a "/etc/skel/" \
          "$encryptedHomesDir/$uid/$user/" &&
        lxc exec "$container" -- \
          chown -R "$user:$user" "$encryptedHomesDir/$uid/$user" &&
        lxc exec "$container" -- \
          chmod 700 "$encryptedHomesDir/$uid/$user" &&
        lxc exec "$container" -- mkdir -p "$decryptedHomesDir/$uid" &&
        lxc exec "$container" -- \
          sshfs -o allow_other -o default_permissions \
          -o IdentityFile=/root/.ssh/id_rsa \
          "root@$ipAdress:$decryptedHomesDirNoSlash/$uid" \
          "$decryptedHomesDir/$uid" &&
        lxc exec deployed -- rm -rf "/home/$user"
      lxc exec master -- rm -rf "/home/$user"
      lxc exec testing -- rm -rf "/home/$user"
      #A softlink to HOME should be prepared on all containers!
      lxc exec deployed -- \
        ln -s "$decryptedHomesDir/$uid/$user" "/home/$user" &&
        lxc exec master -- \
          ln -s "$decryptedHomesDir/$uid/$user" "/home/$user" &&
        lxc exec testing -- \
          ln -s "$decryptedHomesDir/$uid/$user" "/home/$user"
    fi

    #Prepare guacamoles newusers.sh script to add the user there as well
    tmpDir=$(lxc exec "$container" -- mktemp -d --tmpdir="$tmpBaseDir")
    lxc exec "$container" -- sh -c \
      "echo -n $hashedPW > $tmpDir/$user"
    lxc exec "$container" -- chown "$user" "$tmpDir/$user"
    lxc exec "$container" -- mv "$tmpDir/$user" "$updateGuacDir"
    lxc exec "$container" -- rm -rf "$tmpDir"
    lxc exec "$container" -- chmod 400 "$updateGuacDir/$user"

    #Remove the creation request
    shred -u "$file"

    lxc exec guacamole -- sh -c \
      "echo Account ist bereit zur Nutzung. > $statusFile"
  else
    #No Home Image left! Abort and try again next time...
    #We should add some kind of message for the admin here
    echo "Could not create user $user - no home images left!" \
      >>"$logFile"
    lxc exec accounts -- userdel -r "$user"
    lxc exec guacamole -- sh -c
    "echo Account konnte nicht erstellt werden, \
    weil der Speicher nicht auseicht > $statusFile"
  fi

done
