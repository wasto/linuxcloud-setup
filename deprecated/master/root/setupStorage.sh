#!/usr/bin/bash

#/usr/bin/sshfs -o allow_other -o IdentityFile=/root/.ssh/id_rsa root@10.167.130.3:storage /storage #Kann weg sobald quotas gehen

#Zuerst die Extrausers vom account-Container holen:
sshfs -o allow_other -o default_permissions -o IdentityFile=/root/.ssh/id_rsa root@10.167.130.5:/etc /var/lib/extrausers/

/usr/bin/sshfs -o allow_other -o default_permissions -o IdentityFile=/root/.ssh/id_rsa root@10.167.130.3:decryptedhomes /encryptedhomes

for i in $(/usr/bin/ls /encryptedhomes); do
  /usr/bin/mkdir -p /decryptedhomes/$i
  /usr/bin/sshfs -o allow_other -o default_permissions -o IdentityFile=/root/.ssh/id_rsa root@10.167.130.3:decryptedhomes/$i /decryptedhomes/$i
done
