#!/bin/bash

tmpFifo=$(mktemp -u)
updateGuacDir="/updateguac"
title="Passwortwechsel"

setNewPassword() {
  if printf "$1\n$2\n$2" | passwd.orig >/dev/null 2>/dev/null; then
    printf %s "$newPassword" | sha256sum >"$updateGuacDir/$USER" &&
      chmod 400 "$updateGuacDir/$USER" &&
      kdialog --title="$title" --msgbox="Passwort erfolgreich geändert" &&
      exit 0
  else
    kdialog --title="$title" --error="
Passwortwechsel abgelehnt: \
Passwort zu schwach!
Bitte wählen Sie ein starkes Passwort! \
		"
    return 1
  fi
}

retVal=1

while [ $retVal -ne 0 ]; do
  oldPassword=$(kdialog --title="$title" --password="Bitte geben Sie ihr aktuelles Passwort ein") || exit 1
  printf '%s\0' "$oldPassword" >"$tmpFifo" &
  unix_chkpwd "$USER" nullok <"$tmpFifo"
  retVal=$?
  if [ $retVal -ne 0 ]; then
    kdialog --title="$title" --warningcontinuecancel="Falsches Passwort" || exit 1
  fi
done

rm "$tmpFifo"
while true; do
  newPassword=$(kdialog --title="$title" --newpassword="Geben Sie ihr neues Passwort ein.") || exit 1
  setNewPassword "$oldPassword" "$newPassword"
done
