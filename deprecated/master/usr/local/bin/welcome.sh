#!/bin/sh
title="Willkommen"
welcomeTextFile="/var/local/welcome.txt"

kdialog --title="$title" --textbox "$welcomeTextFile" 400 400
kdialog --msgbox="
Herzlich Willkommen auf dem linuxcloud Desktop!

Bitte ändern Sie Ihr Passwort. Sie werden dazu aufgefordert, wenn Sie
dieses Fenster schließen.

Drücken Sie Alt+Ctrl+Shift für das Guacamole Menu. Sie können damit unter
anderem in das und aus dem System copy&pasten.
"

changepassword.sh && rm "$HOME/.config/autostart/welcome.sh.desktop"
