#!/bin/sh

sshfsDir="/root"
ipAdress="10.167.130.3"
encryptedHomesDir="/encryptedhomes"
decryptedHomesDir="/decryptedhomes"

#/usr/bin/sshfs -o allow_other -o IdentityFile=/root/.ssh/id_rsa \
#root@10.167.130.3:storage /storage #Kann weg sobald quotas gehen
#
sshfs -o allow_other -o default_permissions -o IdentityFile=/root/.ssh/id_rsa \
  root@"$ipAdress":"$sshfsDir$decryptedHomesDir" "$encryptedHomesDir"

for directory in "$encryptedHomesDir"/*; do
  mkdir -p "$decryptedHomesDir/$directory"
  sshfs -o allow_other -o default_permissions \
    -o IdentityFile=/root/.ssh/id_rsa \
    root@"$ipAdress":"$sshfsDir$decryptedHomesDir/$directory" \
    "$decryptedHomesDir/$directory"
done
