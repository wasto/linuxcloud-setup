#!/usr/bin/sh
#Erwarteter Parameter: Gesamtzahl der Userhomes die es nachher geben soll.
#(Nicht Anzahl neu zu erstellender User!)
#
encryptedHomesDir="/encryptedhomes"
dencryptedHomesDir="/decryptedhomes"
keyfilesDir="/keyfiles"
keyLength=50
sshfsDir="/root"
firstUID=1000
lastUID=$(($1 + firstUID - 1))
imageSize=2048 # in MB

for uid in $(seq $firstUID $lastUID); do
  if grep -e "$uid".img /etc/fstab >/dev/null; then
    continue
  fi
  dd if=/dev/zero of="$encryptedHomesDir/images/$uid".img bs=1M count="$imageSize"
  mkfs.ext4 "$encryptedHomesDir/images/$uid".img
  mkdir "$encryptedHomesDir/mount/$uid"
  echo "$encryptedHomesDir/images/$uid.img $encryptedHomesDir/mount/$uid" \
    ext4 loop,defaults,_netdev 0 2 >>/etc/fstab
  mount "$encryptedHomesDir/mount/$uid"
  mkdir "$encryptedHomesDir/mount/$uid/$uid"
  chmod -R 777 "$encryptedHomesDir/mount/$uid"
  lxc config device add euler "$uid" disk \
    source="$encryptedHomesDir/mount/$uid/$uid" \
    path="$encryptedHomesDir/$uid" \
    readonly=false
  lxc exec euler -- \
    lxc config device add storage "$uid" disk \
    source="$encryptedHomesDir/$uid" \
    path=/root"$encryptedHomesDir/$uid" \
    readonly=false
  lxc exec euler -- \
    lxc exec storage -- \
    openssl rand -out "$keyfilesDir/$uid" -base64 "$keyLength"
  lxc exec euler -- \
    lxc exec storage -- \
    gocryptfs -passfile "$keyfilesDir/$uid" \
    -config "$keyfilesDir/$uid".conf \
    -init "$sshfsDir$encryptedHomesDir/$uid"
  lxc exec euler -- \
    lxc exec storage -- \
    mkdir "$sshfsDir$dencryptedHomesDir/$uid"
  lxc exec euler -- \
    lxc exec storage -- \
    gocryptfs -passfile "$keyfilesDir/$uid" \
    -config "$keyfilesDir/$uid".conf \
    -force_owner "$uid:$uid" \
    "$sshfsDir$encryptedHomesDir/$uid" \
    "$sshfsDir$dencryptedHomesDir/$uid"
done
