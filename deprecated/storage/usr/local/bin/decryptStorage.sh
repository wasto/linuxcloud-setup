#!/bin/sh

encryptedHomesDir="/encryptedhomes"
decryptedHomesDir="/decryptedhomes"
sshfsDir="/root"
encryptedMountPoint="$sshfsDir$encryptedHomesDir"
decryptedMountPoint="$sshfsDir$decryptedHomesDir"
keyfilesDir="/keyfiles"

#/usr/bin/gocryptfs -passfile /keyfile -config /root/gocrypt/gocryptfs.conf /root/storage-encrypted/ /root/storage/

for directory in "$encryptedMountPoint"/*; do
  uid=$(basename "$directory")
  gocryptfs -passfile "$keyfilesDir/$uid" -config \
    "$keyfilesDir/$uid.conf" -force_owner "$uid:$uid" "$directory" \
    "$decryptedMountPoint/$uid"
done
