#!/bin/sh
usermappingFile="/guacamoleusers/user-mapping.xml"
sed -i "/<authorize *username=\"$1\".*$/,/<\/authorize>.*$/d" \
  "$usermappingFile"
