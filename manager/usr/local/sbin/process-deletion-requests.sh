#!/bin/sh

requestDir="/etc/delete-user-requests"

if [ -z "$(ls $requestDir)" ]; then
  echo "Keine Anfragen gefunden."
  exit 0
fi

for requestFile in "$requestDir"/*; do
  userName=$(stat -c %U "$requestFile")
  userID=$(id -u "$userName")
  if [ "$userID" -lt 1000 ]; then
    echo "UID $userID is too low. Not deleting $userName." >&2
  else
    userdel -r "$userName"
  fi
  rm "$requestFile"
done
