#!/bin/sh

usermappingFile="/var/guacamoleusers/user-mapping.xml"
guacamoleUsers=$(grep -o 'username="[^"]*"' "$usermappingFile" | cut -d '"' -f 2)

for userName in $guacamoleUsers; do
  grep -q "^$userName:" /etc/passwd ||
    delete-guacamole-user "$userName" "$usermappingFile"
done
