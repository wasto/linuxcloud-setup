#!/bin/sh

loginTimesFile="/etc/login-times"
tmpFile="$(mktemp)"

sort -k2 -r "$loginTimesFile" | uniq -f1 > "$tmpFile"
rm "$loginTimesFile"

while read -r line; do
	userName=$(echo "$line" | awk '{print $2}')
	getent passwd "$userName" >/dev/null 2>/dev/null && 
		echo "$line" >> $loginTimesFile
done < "$tmpFile"

