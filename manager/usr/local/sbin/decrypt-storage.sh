#!/bin/sh

encryptedHomesDir="/encryptedhomes"
decryptedHomesDir="/decryptedhomes"
keyfilesDir="/keyfiles"

for directory in "$encryptedHomesDir"/*; do
  uid=$(basename "$directory")
  gocryptfs -passfile "$keyfilesDir/$uid" -config \
    "$keyfilesDir/$uid.conf" -force_owner "$uid:$uid" "$directory" \
    "$decryptedHomesDir/$uid"
done
