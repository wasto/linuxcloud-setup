#!/bin/sh

requestDir="/etc/create-user-requests"
finishedRequests="$requestDir/.meta/finished-requests"
webAppURL="https://webapp-gymms.sbl.ch/linuxcloud/requests"
tempDir=$(mktemp -d)

(
  curl --insecure "$webAppURL/list"
  cat "$finishedRequests"
) | sort |
  uniq -u | xargs -n 1 -I {} -P 5 \
  curl --insecure "$webAppURL/{}" -o "$tempDir/{}"
cp "$tempDir"/* "$requestDir"
rm -r "$tempDir"
