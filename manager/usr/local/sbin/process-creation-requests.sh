#!/bin/sh

statusDir="/var/guacamoleusers/status"
requestDir="/etc/create-user-requests"
privateKey="/root/.ssh/linuxcloud-private.key"
finishedRequests="$requestDir/.meta/finished-requests"

if [ -z "$(ls $requestDir)" ]; then
  echo "Keine Anfragen gefunden."
  exit 0
fi

for request in "$requestDir"/*; do
  sleep 1 #Avoid racing condition
  authTok=$(openssl pkeyutl -decrypt -inkey "$privateKey" -in "$request")
  username=$(echo "$authTok" | cut -f1)
  statusFile="$statusDir/$username"
  password=$(echo "$authTok" | cut -f2)
  timeStamp=$(date +"%d.%m.%Y, %H:%M:%S")
  echo "$timeStamp: Die Anfrage für $username wird vom Server bearbeitet." |
    tee "$statusFile"
  echo "$password" | create-user.sh "$username"
  basename "$request" >>"$finishedRequests"
  rm "$request"
done
