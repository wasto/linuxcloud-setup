#!/bin/sh

loginTimesFile="/etc/login-times"
currentTime=$(date +%s)
expireDays=32
expireSeconds=$((expireDays * 24 * 60 * 60))

sort -k2 -r "$loginTimesFile" | uniq -f1 | \
awk -v currentTime="$currentTime" \
    -v expireSeconds="$expireSeconds" \
    '$1 < currentTime - expireSeconds { print $2 }' |
     xargs -I {} request-user-deletion.sh {}

