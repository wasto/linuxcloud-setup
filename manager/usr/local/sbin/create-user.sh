#!/bin/bash

encryptedHomesDir="/encryptedhomes"
decryptedHomesDir="/decryptedhomes"
statusDir="/var/guacamoleusers/status"
passwdFile="/etc/passwd"
desktopHomesDir="/decryptedhomes"
defaultShell="/bin/bash"
keyfilesDir="/keyfiles"
keyLength=50
resetRequestsDir="/etc/reset-user-requests"
loginTimesFile="/etc/login-times"

statusMsg() {
  #status messages are only shown on webapp if they end with a dot!
  timeStamp=$(date +"%d.%m.%Y, %H:%M:%S")
  echo "$timeStamp: $1" | tee "$statusFile"
  sleep 2
}

bye() {
  sleep 5
  rm "$statusFile"
  exit "$1"
}

abortUser() {
  errorMsg="$1"
  if $isNewUser; then
    userdel -r "$user"
  fi
  statusMsg "$timeStamp: $errorMsg" >&2
  bye 1
}

if [ $# -eq 0 ]; then
  echo "Usage: $0 <username>"
  echo "You can also pipe the desired password into the command"
  exit 1
fi

user="$1"
read -rsp "Passwort for user $user?" password
id "$user" && isNewUser=false || isNewUser=true
statusFile="$statusDir/$user"

if $isNewUser; then
  statusMsg "Der Account $user existiert noch nicht und wird angelegt."
  nextUID=$(
    (
      cut -d: -f3 "$passwdFile" | grep "^....$"
      seq 1000 9999
    ) |
      sort -n | uniq -u | head -1
  )
else
  statusMsg "Der Account $user existiert bereits. Das Passwort wird \
neu gesetzt."
  nextUID=$(id -u "$user")
fi

mkdir -p "$desktopHomesDir/$nextUID/"
echo \
  "$user:$password:$nextUID:$nextUID::$desktopHomesDir/$nextUID/$user:$defaultShell" |
  newusers || abortUser \
  "Fehler beim Anlegen/Abändern des Benutzers $user auf Systemebene."
userID=$(id -u "$user") ||
  abortUser "Beim Abrufen der UID von $user ist ein Fehler aufgetreten."
sed -i "/\b$user\b/d" "$loginTimesFile"
timestamp=$(date "+%s")
echo "$timestamp $user" >>"$loginTimesFile"

if [ -e "$encryptedHomesDir/$userID" ]; then
  #Move the user home into his encrypted container, making sure it's empty
  #beforehand We also delete the old keyfile, remove the old gocryptfs
  #connection and set it up again using a new keyfile. That way any old data
  #we somehow forget to delete become inaccessable.
  if $isNewUser; then
    find "${decryptedHomesDir:?}/${userID:?}/" -mindepth 1 -delete ||
      abortUser "Beim Löschen des alten entschlüsselten persönlichen \
Ordners ist ein Fehler aufgetreten."
    umount -l "$decryptedHomesDir/$userID"
    find "${encryptedHomesDir:?}/${userID:?}/" -mindepth 1 -delete ||
      abortUser "Beim Löschen des alten verschüsselten persönlichen \
Ordners ist ein Fehler aufgetreten."
    rm "${keyfilesDir:?}/${userID:?}"
    rm "${keyfilesDir:?}/${userID:?}.conf"
    (
      openssl rand -out "$keyfilesDir/$userID" -base64 "$keyLength" &&
        gocryptfs -passfile "$keyfilesDir/$userID" -config \
          "$keyfilesDir/$userID".conf -init "$encryptedHomesDir/$userID" &&
        gocryptfs -passfile "$keyfilesDir/$userID" -config \
          "$keyfilesDir/$userID".conf -force_owner \
          "$userID:$userID" "$encryptedHomesDir/$userID" \
          "$decryptedHomesDir/$userID" &&
        mkdir -p "$decryptedHomesDir/$userID/$user" &&
        rsync -aL "/etc/skel/" "$decryptedHomesDir/$userID/$user/" &&
        chown -R "$user:$user" "$decryptedHomesDir/$userID/$user" &&
        chmod 700 "$decryptedHomesDir/$userID/$user" &&
        #Now is the last possible time to let the desktop containers update
        #their quota mounts if needed. We do that by updating a shared file
        #containing all used images and let entr be triggered on the desktop side
        ls "$encryptedHomesDir/" >/etc/homelist
    ) ||
      abortUser "Beim Erzeugen des persönlichen, verschlüsselten Ordners \
ist ein Fehler aufgetreten."
    echo "$user" >"$resetRequestsDir/$user"
    sleep 45 #Wait until the reset request is done
    statusMsg "Der Account $user wurde erfolgreich erstellt und kann ab sofort \
verwendet werden."
    sleep 5
    bye 0
  else
    statusMsg "Das Passwort wurde erfolgreich zurückgesetzt."
    bye 0
  fi
else
  abortUser "Benutzer $user konnte nicht erstellt werden, weil keine \
freien Home-Images mehr vorhanden sind. D.h. der Speicher reicht nicht
aus."
fi
