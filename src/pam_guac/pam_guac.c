#include <libxml/parser.h>
#include <libxml/tree.h>
#include <openssl/evp.h>
#include <openssl/rand.h>
#include <security/pam_appl.h>
#include <security/pam_modules.h>
#include <security/pam_ext.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>

#define usermappingDefaults "/var/guacamoleusers/user-mapping-defaults.xml"

int sha256(const char * password, char ** hash) {
  EVP_MD_CTX * mdctx;
  const EVP_MD * md;
  unsigned char md_value[EVP_MAX_MD_SIZE];
  unsigned int md_len;

  OpenSSL_add_all_digests();
  md = EVP_sha256();

  mdctx = EVP_MD_CTX_new();
  EVP_DigestInit_ex(mdctx, md, NULL);
  EVP_DigestUpdate(mdctx, password, strlen(password));
  EVP_DigestFinal_ex(mdctx, md_value, & md_len);

  EVP_MD_CTX_free(mdctx);

  * hash = (char * ) malloc((md_len * 2 + 1) * sizeof(char));
  if ( * hash == NULL) {
    return PAM_SYSTEM_ERR;
  }

  for (int i = 0; i < md_len; i++) {
    sprintf( * hash + 2 * i, "%02x", md_value[i]);
  }
  return PAM_SUCCESS;
}

int getUID(const char * userName) {
  struct passwd * userEntry = getpwnam(userName);

  if (userEntry == NULL) {
    fprintf(stderr, "Error: User '%s' not found!\n", userName);
    exit(PAM_SYSTEM_ERR);
  }

  return userEntry -> pw_uid;
}

xmlDocPtr parseXML(const char * fileName) {
  xmlDocPtr document = xmlReadFile(fileName, NULL, XML_PARSE_NOBLANKS);
  if (document == NULL) {
    fprintf(stderr, "Error parsing XML file.\n");
    exit(PAM_SYSTEM_ERR);
  }
  return document;
}

xmlNodePtr getXMLRoot(xmlDocPtr document) {
  xmlNodePtr root = xmlDocGetRootElement(document);
  if (root == NULL) {
    fprintf(stderr, "Empty XML file.\n");
    exit(PAM_SYSTEM_ERR);
  }
  return root;
}

bool isUserInXML(xmlNodePtr root,
  const char * userName) {
  for (root = root -> children; root != NULL; root = root -> next) {
    if (root -> type == XML_ELEMENT_NODE &&
      xmlStrcmp(root -> name, (const xmlChar * )
        "authorize") == 0) {
      xmlChar * user = xmlGetProp(root, (const xmlChar * )
        "username");
      if (user != NULL && xmlStrcmp(user, (const xmlChar * ) userName) == 0) {
        xmlFree(user);
        return true;
      }
      xmlFree(user);
    }
  }
  return false;
}

xmlNodePtr updateXMLUserPassword(xmlNodePtr root,
  const char * userName, char * hashedPW) {
  xmlNodePtr authorize;
  for (authorize = root -> children; authorize; authorize = authorize -> next) {
    if (xmlStrcmp(authorize -> name, (const xmlChar * )
        "authorize") == 0) {
      xmlChar * usernameAttr =
        xmlGetProp(authorize, (const xmlChar * )
          "username");

      if (usernameAttr &&
        xmlStrcmp(usernameAttr, (const xmlChar * ) userName) == 0) {
        xmlSetProp(authorize, (const xmlChar * )
          "password",
          (const xmlChar * ) hashedPW);
        xmlFree(usernameAttr);
        break;
      }
      xmlFree(usernameAttr);
    }
  }
  free(hashedPW);
  return root;
}

xmlNodePtr addXMLUser(xmlNodePtr root,
  const char * userName, char * hashedPW) {
  xmlDocPtr tailDoc = parseXML(usermappingDefaults);
  xmlNodePtr newRoot = getXMLRoot(tailDoc);

  // Create the authorize node and set its attributes
  xmlNodePtr authorizeNode = xmlNewChild(root, NULL, BAD_CAST "authorize", NULL);
  xmlNewProp(authorizeNode, BAD_CAST "username", BAD_CAST userName);
  xmlNewProp(authorizeNode, BAD_CAST "password", BAD_CAST hashedPW);
  xmlNewProp(authorizeNode, BAD_CAST "encoding", BAD_CAST "sha256");

  // Copy the child nodes from the new XML document to the authorize node
  xmlNodePtr child = newRoot -> children;
  while (child != NULL) {
    xmlNodePtr newNode = xmlCopyNode(child, 1);
    xmlAddChild(authorizeNode, newNode);

    child = child -> next;
  }
  xmlFreeDoc(tailDoc);
  return root;
}

int saveXMLFile(const char * xmlFile, xmlDocPtr doc) {
  if (xmlSaveFormatFile(xmlFile, doc, 1) == -1) {
    fprintf(stderr, "Error: Could not save the updated XML file.\n");
    xmlFreeDoc(doc);
    return PAM_SYSTEM_ERR;
  }
  xmlFreeDoc(doc);
  return PAM_SUCCESS;
}

PAM_EXTERN int pam_sm_chauthtok(pam_handle_t * pamh, int flags, int argc,
  const char ** argv) {
  const char * password;
  char * hashedPW;
  const char * userName;
  const char * usermappingFile;
  if (argc > 0) {
    usermappingFile = argv[0];
  } else {
    usermappingFile = "/var/guacamoleusers/user-mapping.xml";
  }

  int pamResult;

  if (flags & PAM_UPDATE_AUTHTOK) {
    //get the username
    pamResult = pam_get_user(pamh, & userName, NULL);
    if (pamResult != PAM_SUCCESS) {
      return pamResult;
    }
    //get the password
    pamResult = pam_get_authtok(pamh, PAM_AUTHTOK, & password, NULL);
    if (pamResult != PAM_SUCCESS) {
      return pamResult;
    }
    //hash the password
    pamResult = sha256(password, & hashedPW);
    if (pamResult != PAM_SUCCESS) {
      return pamResult;
    }

    if (getUID(userName) >= 1000) {
      xmlDocPtr doc = parseXML(usermappingFile);
      xmlNodePtr root = getXMLRoot(doc);
      if (isUserInXML(root, userName)) {
        root = updateXMLUserPassword(root, userName, hashedPW);
      } else
        root = addXMLUser(root, userName, hashedPW);
      return saveXMLFile(usermappingFile, doc);
    }
  }
  return PAM_SUCCESS;
}
