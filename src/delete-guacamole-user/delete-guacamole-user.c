#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libxml/parser.h>
#include <libxml/tree.h>

void deleteEntry(xmlNodePtr node,
  const char * userName) {
  xmlNodePtr current = node;
  while (current != NULL) {
    if (current -> type == XML_ELEMENT_NODE && xmlStrcmp(current -> name,
        (const xmlChar * )
        "authorize") == 0) {
      xmlAttrPtr attr = current -> properties;
      while (attr != NULL) {
        if (xmlStrcmp(attr -> name, (const xmlChar * )
            "username") == 0) {
          xmlChar * attrContent =
            xmlNodeListGetString(current -> doc, attr -> children, 1);
          if (attrContent != NULL &&
            xmlStrcmp(attrContent, (const xmlChar * ) userName) == 0) {
            xmlUnlinkNode(current);
            xmlFreeNode(current);
            xmlFree(attrContent); // Free the attribute content
            return;
          }
          xmlFree(attrContent); // Free the attribute content
        }
        attr = attr -> next;
      }
    }
    deleteEntry(current -> children, userName);
    current = current -> next;
  }
}

int main(int argc, char * argv[]) {
  if (argc < 2) {
    printf("Usage: %s <username> [<usermapping-file>]\n", argv[0]);
    return EXIT_FAILURE;
  }

  const char * userName = argv[1];
  const char * usermappingFile;

  if (argc == 3) {
    usermappingFile = argv[2];
  } else {
    usermappingFile = "/var/guacamoleusers/user-mapping.xml";
  }

  xmlDocPtr doc = xmlReadFile(usermappingFile, NULL, XML_PARSE_NOBLANKS);
  if (doc == NULL) {
    fprintf(stderr, "Failed to parse XML file.\n");
    return EXIT_FAILURE;
  }

  xmlNodePtr root = xmlDocGetRootElement(doc);
  deleteEntry(root, userName);

  if (xmlSaveFormatFile(usermappingFile, doc, 1) == -1) {
    fprintf(stderr, "Failed to save XML file.\n");
  }

  xmlFreeDoc(doc);
  xmlCleanupParser();

  return EXIT_SUCCESS;
}
