<?php

function generateRandomPassword($length) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $password = '';
    $maxIndex = strlen($characters) - 1;
    for ($i = 0; $i < $length; $i++) {
        $password .= $characters[random_int(0, $maxIndex)];
    }
    return $password;
}

$domain = "linuxcloud.ch";
$requestsDir = "../requests";
$listFilePath = $requestsDir."/list";
$username = $_SERVER['PHP_AUTH_USER'];
$password = generateRandomPassword(9);
$requestFilePath = tempnam($requestsDir,time());
$requestFileName = basename($requestFilePath);

$dataToEncrypt = "$username $password";
$publicKey = file_get_contents('publicKey.pem');
openssl_public_encrypt("$username $password", $encryptedData, $publicKey);
file_put_contents($requestFilePath, $encryptedData);
$listFile=fopen($listFilePath,"a");
fwrite($listFile,$requestFileName);
fclose($listFile);

echo "Der Account auf https://$domain wird eingerichtet. Es kann bis zu 5 Minuten dauern, bis alles bereit ist!<br>";
echo "Danach können Sie sich auf <a href=\"https://$domain\">https://$domain</a> einloggen.<br>";
echo "<div id=authtok class=card>";
echo "Benutzername: $username<br>";
echo "Passwort: $password";
echo "</div>";
echo "<p>";
echo "Schreiben Sie sich das Passwort auf oder kopieren Sie es! Es wird nur solange angezeigt bis Sie die Seite verlassen! Sollten Sie diese Seite neu laden, so wird ein neues Passwort generiert!";
echo "<p>";
echo "Falls das nicht funktioniert, so ist der Server gerade nicht erreichbar oder es gibt zur Zeit keinen freien Platz mehr für einen neuen Account. Versuchen Sie es in diesem Fall später nochmals.";
echo "<p>";

?>
