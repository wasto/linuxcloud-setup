<?php

function generateRandomPassword($length) {
   $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
   $password = '';
   $maxIndex = strlen($characters) - 1;
   for ($i = 0; $i < $length; $i++) {
     $password .= $characters[random_int(0, $maxIndex)];
   }
   return $password;
}

$domain = "linuxcloud.ch";
$requestsDir = "../requests";
$listFilePath = $requestsDir."/list";
$username = $_SERVER['PHP_AUTH_USER'];
$password = generateRandomPassword(12);
$requestFilePath = tempnam($requestsDir,time());
$requestFileName = basename($requestFilePath);
$publicKey = file_get_contents('linuxcloud-public.pem');

openssl_public_encrypt("$username\t$password", $encryptedAuthTok, $publicKey);
file_put_contents($requestFilePath, $encryptedAuthTok);
file_put_contents($listFilePath,$requestFileName."\n",FILE_APPEND);

echo "<p>Benutzername: <span id=\"username\">$username</span></p>";
echo "<p>Passwort: <span id=\"password\">$password</span></p>";
?>
