const statusMessages = [];

function fetchStatus() {
  fetch('status.php')
    .then(response => response.text())
    .then(data => {
      data=data.trim();
      if (data.endsWith(".") && statusMessages[statusMessages.length - 1] !==
        data) {
        statusMessages.push(data);
        updateStatusList();
      }
    })
    .catch(error => {
      console.error('Error fetching status:', error);
    });
  setTimeout(fetchStatus, 1000);
}

function updateStatusList() {
  const statusList = document.getElementById('statusList');
  statusList.innerHTML = '';
  for (const message of statusMessages) {
    const listItem = document.createElement('li');
    listItem.textContent = message;
    statusList.appendChild(listItem);
  }
}

function updateAuthTok() {
  const infoOutput = document.getElementById('authtok');
  fetch('authtok.php')
    .then(response => response.text())
    .then(data => {
      infoOutput.innerHTML = data;
    });
  infoOutput.classList.add("loaded");
}

function initStatus() {
  const options = {
    year: 'numeric',
    month: '2-digit',
    day: '2-digit',
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit',
    hour12: false,
  };
  const locale = 'de-CH';
  const timeStamp = new Date()
    .toLocaleString(locale, options);
  statusMessages.push(timeStamp + ": Die Anfrage wird an den Server gesendet.");
}
initStatus();
updateStatusList();
updateAuthTok();
fetchStatus();
