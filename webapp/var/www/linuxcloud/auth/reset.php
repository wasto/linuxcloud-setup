<?php

$domain = "linuxcloud.ch";
$requestsDir = "../reset-requests";
$listFilePath = $requestsDir."/list";
$username = $_SERVER['PHP_AUTH_USER'];

$requestFilePath = tempnam($requestsDir,time());
$requestFileName = basename($requestFilePath);


file_put_contents($requestFilePath, $username);
file_put_contents($listFilePath,$requestFileName."\n",FILE_APPEND);

echo "Anfrage gesendet! Ihre Desktop-Einstellungen werden innerhalb einer";
echo "Minute zurückgesetzt. Falls Sie schon eingeloggt sind, werden Sie die";
echo "Verbindung zum Desktop kurzzeitig verlieren.";
echo "Anschliessend können Sie sich wieder neu anmelden. Ihr Desktop";
echo "sollte dann wieder normal funktionieren.";
?>
