<?php

$username = $_SERVER['PHP_AUTH_USER'];
$url = "https://status.linuxcloud.ch/status/".$username;

/* Ignoring SSL Certificate */
$context = stream_context_create([
    'ssl' => [
        'verify_peer' => false,
        'verify_peer_name' => false,
    ],
]);

echo file_get_contents($url,false,$context);
?>
